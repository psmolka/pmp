import { Component } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { AuthenticationService } from "./services";
import { Observable } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  get loggedIn() { return localStorage.getItem("currentUser") ? true : false};

  constructor(
    private titleService: Title,
    private authenticationService: AuthenticationService
  ) {
  }

  onInit() {
    this.titleService.setTitle("PMP");
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  logout() {
    this.authenticationService.logout();
  }
}
