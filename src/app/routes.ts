import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TasksComponent } from './tasks/tasks.component';
import { E404Component } from './e404/e404.component';
import { AuthGuard } from "./guards/index";

const routes = [
  {
    path: "",
    component: WelcomeComponent
  }, {
    path: "login",
    component: LoginComponent
  }, {
    path: "register",
    component: RegisterComponent
  }, {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard]
  }, {
    path: "tasks",
    component: TasksComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    component: E404Component
  }
];

export { routes };
