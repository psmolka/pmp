import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router } from "@angular/router";
import { MdIconModule } from "@angular/material";

import { AuthenticationService } from './authentication.service';

let routerStub;

describe('Authentication service', () => {
  beforeEach(() => {
    routerStub = {
      navigate: jasmine.createSpy("navigate")
    }
    TestBed.configureTestingModule({
      imports: [ HttpModule, MdIconModule ],
      providers: [
        AuthenticationService,
        {
          provide: Router,
          useValue: routerStub
        }
      ]
    });
  });

  it('should be created', inject([ AuthenticationService ], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));
});
