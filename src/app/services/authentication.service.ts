import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

const domain="http://dev.smolka.it";

@Injectable()
export class AuthenticationService {
  public token: string;

  constructor(
    private http: Http,
    private router: Router
  ) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  register(username: string, password: string): Observable<boolean> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(domain + "/user/add", "name=" + username +"&password=" + password, {headers: headers})
      .map((response: Response) => {
        if(response.ok) {
          return true;
        } else {
          return false;
        }
      });
  }

  login(username: string, password: string): Observable<boolean> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(domain + "/token", "name=" + username +"&password=" + password, {headers: headers})
      .map((response: Response) => {
        if (response.ok) {
          this.token = response.text();
          localStorage.setItem('currentUser', JSON.stringify({ name: username, token: this.token }));
          return true;
        } else {
          return false;
        }
      });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }
}
