import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  tasks = [{
    id: 2,
    title: "2"
  }, {
    id: 1,
    title: "1"
  }];

  isEditor = function() {
    return "true";
  }

  snack(text: string) {
    return this.snackBar.open(text, "X", {duration: 1500});
  }

  addTask() {
    const newId = this.tasks.length+1;
    this.tasks.unshift({id: newId, title: String(newId)});
  }

  send() {
    this.snack("Sent!");
  }

  like() {
    this.snack("Liked!");
  }

  test() {
    this.snack("Tested!");
  }

  clear(area) {
    this.snack("Cleaed!");
    area.value = "";
  }

  constructor(public snackBar: MdSnackBar, private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle("Tasks");
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

}
