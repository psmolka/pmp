import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MdIconModule } from "@angular/material";
import { MdInputModule } from "@angular/material";
import { MdSnackBarModule } from "@angular/material";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TasksComponent } from './tasks.component';

describe('TasksComponent', () => {
  let component: TasksComponent;
  let fixture: ComponentFixture<TasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        MdIconModule,
        MdInputModule,
        MdSnackBarModule
      ],
      declarations: [ TasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
