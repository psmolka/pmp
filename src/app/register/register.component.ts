import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { AuthenticationService } from "../services/index";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  model: any = {};
  loading = false;
  error = "";

  constructor(
    private titleService: Title,
    private router: Router,
    private authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.titleService.setTitle("Register");
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }



  register() {
    this.loading = true;
    this.authenticationService.register(this.model.username, this.model.password)
      .subscribe(result => {
        if (result === true) {
          this.router.navigate(['/']);
          this.loading = false;
        } else {
          this.error = 'Username or password is incorrect';
          this.loading = false;
        }
      },
      (error: any) => {
        this.error = String(error);
        this.loading = false;
      }
    );
  }
}
