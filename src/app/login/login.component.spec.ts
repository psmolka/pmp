import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

import { AuthenticationService } from "../services/authentication.service";
import { LoginComponent } from './login.component';

const RouterStub: object = {
  navigate(...any): boolean {
    any = null;
    return true;
  }
};

const AuthenticationServiceStub: object = {};

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        FormsModule,
      ],
      declarations: [ LoginComponent ],
      providers: [
        {
          provide: Router,
          useValue: RouterStub
        },
        {
          provide: AuthenticationService,
          useValue: AuthenticationServiceStub
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
