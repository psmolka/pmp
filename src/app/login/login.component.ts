import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { AuthenticationService } from "../services/index";
import { MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  error = "";

  constructor(
    private titleService: Title,
    private router: Router,
    public  snackBar: MdSnackBar,
    private authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.titleService.setTitle("Login");
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(result => {
        if (result === true) {
          this.router.navigate(['/']);
        } else {
          this.error = 'Username or password is incorrect';
          this.loading = false;
        }
      },
      (error: any) => {
        this.loading = false;
        this.error = 'Username or password is incorrect';
        this.snackBar.open(this.error, "X", { duration: 1500 });
      }
    );
  }
}
