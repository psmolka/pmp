import { TestBed, async } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '@angular/material';
import { AppComponent } from './app.component';
import { AuthenticationService } from "./services/authentication.service";

describe('AppComponent', () => {
  beforeEach(async(() => {
    const AuthenticationServiceStub = {};
    TestBed.configureTestingModule({
      imports: [ MaterialModule, RouterTestingModule],
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provide: AuthenticationService,
          useValue: AuthenticationServiceStub
        }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should have layout with md-sidenav', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("md-sidenav-container")).toBeTruthy();
    expect(compiled.querySelector("md-sidenav")).toBeTruthy();
  }));

  it('should render logo and menu buttons', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('md-icon').length).toEqual(2);
  }));
});
