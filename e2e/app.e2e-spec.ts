import { LonePage } from './app.po';

describe('lone App', () => {
  let page: LonePage;

  beforeEach(() => {
    page = new LonePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
